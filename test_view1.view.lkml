# view: test_view1 {
#   # # You can specify the table name if it's different from the view name:
#   # sql_table_name: my_schema_name.tester ;;
#   #
#   # # Define your dimensions and measures here, like this:
#   # dimension: user_id {
#   #   description: "Unique ID for each user that has ordered"
#   #   type: number
#   #   sql: ${TABLE}.user_id ;;
#   # }
#   #
#   # dimension: lifetime_orders {
#   #   description: "The total number of orders for each user"
#   #   type: number
#   #   sql: ${TABLE}.lifetime_orders ;;
#   # }
#   #
#   # dimension_group: most_recent_purchase {
#   #   description: "The date when each user last ordered"
#   #   type: time
#   #   timeframes: [date, week, month, year]
#   #   sql: ${TABLE}.most_recent_purchase_at ;;
#   # }
#   #
#   # measure: total_lifetime_orders {
#   #   description: "Use this for counting lifetime orders across many users"
#   #   type: sum
#   #   sql: ${lifetime_orders} ;;
#   # }
# #   dimension: sample_dimenshion {
# #     description: "sample value for view"
# #     type: number
# #     sql:  ${TABLE}.value ;;
# #     }
# derived_table: {
#   sql: select * from dam_test_db3.dam_test_sch1.sample1 ;;
# }
# }
# # view: test_view1 {
# #   # Or, you could make this view a derived table, like this:
# #   derived_table: {
# #     sql: SELECT
# #         user_id as user_id
# #         , COUNT(*) as lifetime_orders
# #         , MAX(orders.created_at) as most_recent_purchase_at
# #       FROM orders
# #       GROUP BY user_id
# #       ;;
# #   }
# #
# #   # Define your dimensions and measures here, like this:
# #   dimension: user_id {
# #     description: "Unique ID for each user that has ordered"
# #     type: number
# #     sql: ${TABLE}.user_id ;;
# #   }
# #
# #   dimension: lifetime_orders {
# #     description: "The total number of orders for each user"
# #     type: number
# #     sql: ${TABLE}.lifetime_orders ;;
# #   }
# #
# #   dimension_group: most_recent_purchase {
# #     description: "The date when each user last ordered"
# #     type: time
# #     timeframes: [date, week, month, year]
# #     sql: ${TABLE}.most_recent_purchase_at ;;
# #   }
# #
# #   measure: total_lifetime_orders {
# #     description: "Use this for counting lifetime orders across many users"
# #     type: sum
# #     sql: ${lifetime_orders} ;;
# #   }
# # }
view: test_view2{
  derived_table: {
    sql: select * from dam_test_db3.dam_test_sch1.sample1
      ;;
  }

  measure: count {
    type: count
    drill_fields: [detail*]
  }

  dimension: name {
    type: string
    primary_key: yes
    sql: ${TABLE}."NAME";;
  }

  dimension: price {
    type: number
    sql: ${TABLE}."PRICE" ;;
  }

  set: detail {
    fields: [name, price]
  }
}
